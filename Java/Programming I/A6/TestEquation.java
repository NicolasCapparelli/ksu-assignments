package A6;
/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Assignment #: 6
 */


// TODO: Roots not working correctly
public class TestEquation {

    public static void main(String[] args) {

        QuadraticEquation[] equations = {new QuadraticEquation(3,8,4), new QuadraticEquation(3,4,8), new QuadraticEquation(2,8,2)};

        for (QuadraticEquation equation : equations){
            System.out.println("a = " + equation.getA());
            System.out.println("b = " + equation.getB());
            System.out.println("c = " + equation.getC());

            if (equation.getRoot1() != 0){
                System.out.println("Root 1 = " + equation.getRoot1());
            } else {
                System.out.println("Root 1 is Undefined");
            }

            if (equation.getRoot1() != 0){
                System.out.println("Root 1 = " + equation.getRoot2());
            } else {
                System.out.println("Root 1 is Undefined");
            }

            System.out.println();
            System.out.println();
        }

    }
}

