package A6;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Assignment #: 6
 */

// TODO: Roots not working correctly
public class QuadraticEquation {

    private int a;
    private int b;
    private int c;

    QuadraticEquation(int ca, int cb, int cc){
        this.a = ca;
        this.b = cb;
        this.c = cc;
    }

    // Methods

    private int getDiscriminant(){
        return (this.b*this.b) - 4*this.a*this.c;
    }

    public double getRoot1(){

        int disc = getDiscriminant();

        if (disc > 0){
            return ((this.b * -1) + Math.sqrt(disc)) / (2*this.a);

        } else {
            return 0;
        }
    }

    public double getRoot2(){
        int disc = getDiscriminant();

        if (disc > 0){
            return ((this.b * -1) - Math.sqrt(disc)) / (2*this.a);
        } else {
            return 0;
        }

    }

    // Getters
    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }
}
