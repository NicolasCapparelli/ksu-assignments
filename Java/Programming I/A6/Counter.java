package A6;
/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Assignment #: 6
 */


public class Counter {
    private int value = 0;

    public void increment(){
        this.value++;
    }

    public int getValue() {
        return value;
    }
}
