package A6;
import java.util.Random;
/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Assignment #: 6
 */

public class CounterTest {

    public static void main(String[] args) {

        Counter heads = new Counter();
        Counter tails = new Counter();

        Random coin = new Random();

        for (int i = 0; i < 100; i++){
            if (coin.nextBoolean()){
                heads.increment();
            } else {
                tails.increment();
            }
        }

        System.out.println("Heads: " + heads.getValue());
        System.out.println("Tails: " + tails.getValue());
    }
}
