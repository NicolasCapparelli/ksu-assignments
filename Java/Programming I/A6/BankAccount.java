package A6;
import java.util.Date;
/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Assignment #: 6
 */

// TODO: Double check interest rate
public class BankAccount {

    private int id;
    private double balance;
    private double annualInterestRate;
    private Date dateCreated = new Date();

    BankAccount(){
        this.id = 0;
        this.balance = 0.0;
        this.annualInterestRate = 0.0;
        this.dateCreated.getTime();
    }

    BankAccount(int newId, int newBalance){
        this.id = newId;
        this.balance = newBalance;
        this.annualInterestRate = 0.0;
        this.dateCreated.getTime();
    }


    public String getMonthlyInterestRate(){
        return String.format("%.2f", annualInterestRate / 12) + "%";
    }

    public String getMonthlyInterest(){
        return "$" + String.format("%.2f", (balance * (annualInterestRate / 12)));
    }

    public void withdraw(double amount){
        this.balance -= amount;
    }

    public void deposit(double amount){
        this.balance += amount;
    }

    @Override
    public String toString(){
        System.out.println("Account ID:\t\t\t" + this.id);
        System.out.println("Account Balance:\t" + "$" + getBalance());
        System.out.println("Interest Rate:\t\t" + String.format("%.2f", this.annualInterestRate) + "%");
        System.out.println("Date Opened:\t\t" + this.dateCreated);

        return null;
    }



    public Date getDateCreated() {
        return dateCreated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public void setAnnualInterestRate(double annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }
}
