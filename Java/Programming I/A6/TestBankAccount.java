package A6;
/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Assignment #: 6
 */

public class TestBankAccount {

    public static void main(String[] args) {
        BankAccount myObject = new BankAccount(123456, 10000);
        myObject.setAnnualInterestRate(2.5);
        myObject.withdraw(3500);
        myObject.deposit(500);

        System.out.println("$" + myObject.getBalance());
        System.out.println(myObject.getMonthlyInterest());
        System.out.println(myObject.getDateCreated());
    }
}
