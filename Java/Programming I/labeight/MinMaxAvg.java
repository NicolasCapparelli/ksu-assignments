package labeight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 8
 */
public class MinMaxAvg {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int z = scanner.nextInt();

        System.out.println("You entered: " + x + ", "+  y + ", " + z);
        System.out.println("Max value: " + max(x,y,z));
        System.out.println("Min value: " + min(x,y,z));
        System.out.println("Average value: " + average(x,y,z));

    }

    private static int max(int x, int y, int z){
        return Collections.max(new ArrayList<>(Arrays.asList(x,y,z)));
    }

    private static int min(int x, int y, int z){
        return Collections.min(new ArrayList<>(Arrays.asList(x,y,z)));
    }

    private static double average(int x, int y, int z){
        double p = x + y + z;

        return p/3;
    }
}