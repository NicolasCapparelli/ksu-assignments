package labeight;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 8
 */

public class PalindromeInteger {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int inputNumber = scanner.nextInt();

        System.out.println("Entered value: " + inputNumber);

        if (isPalindrome(inputNumber))
            System.out.println("Judgement: Palindrome");
        else
            System.out.println("Judgement: Not Palindrome");

    }

    private static int reverse(int number){
        int reverse = 0;

        while(number != 0) {
            reverse = reverse * 10;
            reverse = reverse + number % 10;
            number = number / 10;
        }
        return reverse;
    }

    private static boolean isPalindrome(int number){
        return number == reverse(number);
    }
}
