package labeight;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 8
 */

public class ComputeAreas {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        double sqSide = scanner.nextDouble();
        System.out.println("Square side: " + sqSide);
        System.out.println("Square area: " + (float)squareArea(sqSide));

        double recW = scanner.nextDouble();
        double recL = scanner.nextDouble();
        System.out.println("pastlabs.Rectangle width: " + recW);
        System.out.println("pastlabs.Rectangle length: " + recL);
        System.out.println("pastlabs.Rectangle area: " + (float)rectangleArea(recW, recL));

        double radius = scanner.nextDouble();
        System.out.println("Circle radius: " + radius);
        System.out.println("Circle area: " + (float)circleArea(radius));

        double base = scanner.nextDouble();
        double triH = scanner.nextDouble();
        System.out.println("Triangle base: " + base);
        System.out.println("Triangle height: " + triH);
        System.out.println("Triangle area: " + (float)triangleArea(base, triH));
    }

    private static double squareArea(double side){
        return side * side;
    }

    private static double rectangleArea(double width, double length){
        return length * width;
    }

    private static double circleArea(double radius){
        return Math.PI * (radius*radius);
    }

    private static double triangleArea(double base, double height){
        return (.5 * base) * height;
    }
}
