package lab11;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 11
 */

public class CircleTest {

    public static void main(String[] args) {

        Circle circle = new Circle();

        System.out.println(circle);

        System.out.println("The area is: " + String.format("%.2f", circle.getArea()));
        System.out.println("The perimeter is: " + String.format("%.2f", circle.getPerimeter()));

        circle.setRadius(10);

        System.out.println(circle);
        System.out.println("The area is: " + String.format("%.2f", circle.getArea()));
        System.out.println("The perimeter is: " + String.format("%.2f", circle.getPerimeter()));

    }
}
