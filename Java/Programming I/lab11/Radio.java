package lab11;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 11
 */
public class Radio {

    private int station = 1;
    private int volume = 1;

    private boolean on = false;

    public Radio(){

    }

    public int getStation() {
        return station;
    }

    public int getVolume() {
        return volume;
    }

    public void turnOn(){
        this.on = true;
    }

    public void turnOff(){
        this.on = false;
    }

    public void stationUp(){
        if (this.station < 10 && this.on)
            this.station++;
    }

    public void stationDown(){
        if (this.station > 1 && this.on)
            this.station--;
    }

    public void volumeUp(){
        if (this.volume < 10 && this.on)
            this.volume++;
    }

    public void volumeDown(){
        if (this.volume > 1 && this.on)
            this.volume--;
    }

    @Override
    public String toString(){
        if (this.on)
            return "The radio station is " + this.station + " and the volume is " + this.volume;
        else
            return "The radio is off";
    }

}
