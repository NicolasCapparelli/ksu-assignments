package lab11;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 11
 */

public class Circle {

    private double radius = 1;

    public Circle(){
    }

    public Circle(double r){
        this.radius = r;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return  Math.PI * (this.radius*this.radius);
    }

    public double getPerimeter(){
        return 2*Math.PI*this.radius;
    }

    @Override
    public String toString(){
        return "The circle has radius " + this.radius;
    }

}
