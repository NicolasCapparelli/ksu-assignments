package lab11;
/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 11
 */

public class RadioTest {

    public static void main(String[] args) {
        Radio radio = new Radio();
        radio.turnOn();

        System.out.println(radio);

        radio.volumeUp();
        radio.volumeUp();
        radio.volumeUp();
        System.out.println(radio);

        radio.stationUp();
        radio.stationUp();
        radio.stationUp();
        radio.stationUp();
        radio.stationUp();
        System.out.println(radio);

        radio.volumeDown();
        System.out.println(radio);

        radio.stationUp();
        radio.stationUp();
        radio.stationUp();
        System.out.println(radio);

        radio.turnOff();
        System.out.println(radio);

        radio.volumeUp();
        radio.volumeUp();
        System.out.println(radio);


        radio.stationDown();
        radio.stationDown();
        System.out.println(radio);
    }
}
