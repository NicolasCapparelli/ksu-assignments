package Lab13;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 13
 */

public class AddMatrices {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[][] matrixA = new int[3][3];
        int[][] matrixB = new int[3][3];

        System.out.println("Enter Matrix A");

        for (int i = 0; i < matrixA.length; i++){
            for (int j = 0; j < matrixA[0].length; j++){
                matrixA[i][j] = scanner.nextInt();
            }

        }

        System.out.println("Enter Matrix B");

        for (int i = 0; i < matrixB.length; i++){
            for (int j = 0; j < matrixB[0].length; j++){
                matrixB[i][j] = scanner.nextInt();
            }
        }

        int[][] answers = addition(matrixA, matrixB);
        printMatrix(matrixA, "Matrix A");
        printMatrix(matrixB, "Matrix B");
        printMatrix(answers, "Matrix A + B");
    }

    private static int[][] addition(int[][] matA, int[][] matB){

        if (matA.length != matB.length && matA[0].length != matB[0].length){
            System.out.println("Please ensure both matrices have the same number of rows and columns");
            System.exit(1);
        }

        int[][] matAB = new int[matA.length][matA[0].length];

        for (int i = 0; i < matA.length; i++) {
            for (int j = 0; j < matA.length; j++) {
                matAB[i][j] = matA[i][j] + matB[i][j];
            }
        }

        return matAB;
    }

    // Prints matrix passed in to reduce redundancy
    private static void printMatrix(int[][] printMe, String name){

        System.out.println(name + ":");
        for (int[] value : printMe) {
            for (int j = 0; j < printMe[0].length; j++) {
                System.out.print("\t" + value[j]);
            }
            System.out.print("\n");
        }

        System.out.print("\n");
    }
}
