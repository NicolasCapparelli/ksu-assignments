package Lab13;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 13
 */

public class LocateLargestElement {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[][] matrix = new int[3][4];

        System.out.println("Enter Matrix:");

        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 4; j++){
                matrix[i][j] = scanner.nextInt();
            }
        }

        System.out.println("The entered matrix:");
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 4; j++){
                System.out.print("\t" + matrix[i][j]);
            }
            System.out.print("\n");
        }

        int[] location = locateLargest(matrix);

        System.out.println("The largest value is located at row " + location[0] + " and column " + location[1]);

    }

    private static int[] locateLargest(int[][] field){

        int max = 0;
        int[] location = new int[2];

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
               if (field[i][j] > max) {
                   max = field[i][j];
                   location[0] = i;
                   location[1] = j;
               }
            }
        }

        return location;
    }
}
