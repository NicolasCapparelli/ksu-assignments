package Lab13;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 13
 */

public class SumArrayColumns {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[][] matrix = new int[3][4];

        System.out.println("Enter Matrix:");
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 4; j++){
                matrix[i][j] = scanner.nextInt();
            }
        }

        System.out.println("The entered matrix:");
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 4; j++){
                System.out.print("\t" + matrix[i][j]);
            }
            System.out.print("\n");
        }

        int[] answers = sumColumn(matrix);

        for (int i = 0; i < answers.length; i++){
            System.out.print("\nSum of column " + i + " is " + answers[i]);
        }
    }


    private static int[] sumColumn(int[][] matrix){

        int[] sums = new int[matrix[0].length];

        for (int[] col : matrix) {
            for (int j = 0; j < col.length; j++) {
                sums[j] += col[j];
            }
        }

        return sums;
    }
}
