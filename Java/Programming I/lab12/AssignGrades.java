package lab12;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 12
 */

public class AssignGrades {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int classSize = scanner.nextInt();
        int[] grades = new int[classSize];

        for(int i = 0; i < classSize; i++){
            int g = -1;
            while(g < 0 || g > 100){
                g = scanner.nextInt();
                if (g < 0 || g > 100)
                    System.out.println("Please input a grade between 0 and 100");
            }
            grades[i] = g;
        }

        System.out.println("Class size: " + classSize);
        System.out.print("Entered Grades: ");
        for (int i = 0; i < classSize; i++){
            System.out.print(grades[i]);
            if (i < classSize - 1)
                System.out.print(", ");
        }
        System.out.println();

        printGrades(grades);
    }

    private static void printGrades(int[] grades){

        int c = 0;
        for (int g : grades){

            System.out.print("\nStudent " + c + " score is " + g + " and grade is ");
            if (g >= 90 && g <= 100)
                System.out.print("A");
            else if (g >= 80 && g <= 89)
                System.out.print("B");
            else if (g >= 70 && g <= 79)
                System.out.print("C");
            else if (g >= 60 && g <= 69)
                System.out.print("D");
            else
                System.out.print("F");
            c++;
        }
    }

}
