package lab12;

import java.util.Arrays;
import java.util.Random;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 12
 */

public class ArrayMethods {

    public static void main(String[] args) {

        int[] randArray = new int[5];
        Random dice = new Random();

        System.out.print("Original array: ");
        for (int i = 0; i < randArray.length; i++){
            randArray[i] = dice.nextInt((100 - 1) + 1) + 1;
            System.out.print(randArray[i]);

            if (i < randArray.length - 1)
                System.out.print(", ");
        }

        System.out.print("\nMax value:\t" + arrayMax(randArray));
        System.out.println("\nMin value:\t" + arrayMin(randArray));
        arraySquared(randArray);
        arrayReversed(randArray);


    }

    private static int arrayMax(int[] par){
        int[] p = par.clone();
        Arrays.sort(p);
        return p[p.length - 1];
    }

    private static int arrayMin(int[] par){
        int[] p = par.clone();
        Arrays.sort(p);
        return p[0];
    }

    private static void arraySquared(int[] par){
        System.out.print("Squared Array:\t");
        for (int i = 0; i < par.length; i++){
            par[i] = par[i] * par[i];
            System.out.print(par[i]);
            if (i < par.length - 1)
                System.out.print(", ");
        }
    }

    private static void arrayReversed(int[] par){
        System.out.print("\nReversed array:\t");
        for (int i = par.length - 1; i > -1; i--){
            System.out.print(par[i]);
            if (i > 0)
                System.out.print(", ");
        }
    }
}
