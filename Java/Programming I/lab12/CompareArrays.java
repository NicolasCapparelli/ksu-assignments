package lab12;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 12
 */

public class CompareArrays {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int arrSize = scanner.nextInt();
        int[] aOne = new int[arrSize];
        int[] aTwo = new int[arrSize];

        for (int i = 0; i < aOne.length; i++){
            aOne[i] = scanner.nextInt();
        }

        for (int i = 0; i < aTwo.length; i++){
            aTwo[i] = scanner.nextInt();
        }

        System.out.println("Array Size:\t\t" + arrSize);

        System.out.print("First array:\t");
        for (int i = 0; i < aOne.length; i++){
            System.out.print(aOne[i]);

            if (i < aOne.length - 1)
                System.out.print(", ");
        }

        System.out.print("\nSecond array:\t");
        for (int i = 0; i < aTwo.length; i++){
            System.out.print(aTwo[i]);

            if (i < aTwo.length - 1)
                System.out.print(", ");
        }

        System.out.print("\nJudgement:\t\t");
        if (compare(aOne, aTwo))
            System.out.println("The arrays are identical");
        else
            System.out.println("The arrays are not identical");
    }

    private static boolean compare(int[] one, int[] two){
        return Arrays.equals(one, two);
    }
}
