package lab10;
/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 10
 */

public class StockTest {
    public static void main(String[] args) {

        Stock goog = new Stock("Google", "GOOG");
        goog.setPreviousClosingPrice(134.67);
        goog.setCurrentPrice(131.98);

        Stock msft = new Stock("Microsoft", "MSFT");
        msft.setPreviousClosingPrice(156.52);
        msft.setCurrentPrice(161.22);

        System.out.println("Google Stock");
        System.out.println("\tSymbol:\t" + goog.getSymbol());
        System.out.println("\tClosing price:\t" + goog.getPreviousClosingPrice());
        System.out.println("\tCurrent price:\t" + goog.getCurrentPrice());
        System.out.println("\tChange percent:\t" + goog.getChangePercent());
        System.out.println("\t" + goog);

        System.out.println();

        System.out.println("Microsoft Stock");
        System.out.println("\tSymbol:\t" + msft.getSymbol());
        System.out.println("\tClosing price:\t" + msft.getPreviousClosingPrice());
        System.out.println("\tCurrent price:\t" + msft.getCurrentPrice());
        System.out.println("\tChange percent:\t" + msft.getChangePercent());
        System.out.println("\t" + msft);

    }
}
