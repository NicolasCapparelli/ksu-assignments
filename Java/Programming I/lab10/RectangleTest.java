package lab10;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 10
 */
public class RectangleTest {
    public static void main(String[] args) {

        Rectangle firstObj = new Rectangle();
        Rectangle secondObj = new Rectangle(5, 6);

        System.out.println("First object");
        System.out.println("\tHeight:\t\t" + firstObj.getHeight() + " unit");
        System.out.println("\tWidth:\t\t" + firstObj.getWidth() + " unit");
        System.out.println("\tArea:\t\t" + firstObj.getArea()+ " unit");
        System.out.println("\tPerimeter:\t" + firstObj.getPerimeter() + " units");

        System.out.println();

        System.out.println("Second Object");
        System.out.println("\tHeight:\t\t" + secondObj.getHeight() + " unit");
        System.out.println("\tWidth:\t\t" + secondObj.getWidth() + " unit");
        System.out.println("\tArea:\t\t" + secondObj.getArea() + " units");
        System.out.println("\tPerimeter:\t" + secondObj.getPerimeter() + " units");
    }
}
