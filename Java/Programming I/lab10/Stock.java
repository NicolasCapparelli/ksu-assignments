package lab10;
/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 10
 */

public class Stock {
    private String symbol;
    private String name;
    private double previousClosingPrice;
    private double currentPrice;

    public Stock(String n, String s){
        this.name = n;
        this.symbol = s;

    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public double getPreviousClosingPrice() {
        return previousClosingPrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setPreviousClosingPrice(double previousClosingPrice) {
        this.previousClosingPrice = previousClosingPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getChangePercent(){

        String mod;

        if (this.previousClosingPrice < this.currentPrice)
            mod = "";
        else
            mod = "-";

        return mod + String.format("%.2f", ((previousClosingPrice - currentPrice) / previousClosingPrice) * 100) + "%";
    }


    @Override
    public String toString(){
        return this.name + " stock's closing price is $" + this.currentPrice;

    }
}
