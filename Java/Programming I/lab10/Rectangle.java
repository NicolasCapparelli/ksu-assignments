package lab10;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 10
 */

public class Rectangle {

    private double height;
    private double width;

    public Rectangle(){
        this.height = 1;
        this.width = 1;
    }

    public Rectangle(double h, double w){
        this.height = h;
        this.width = w;
    }

    public double getArea(){
        return this.height * this.width;
    }

    public double getPerimeter(){
        return (this.height * 2) + (this.width * 2);
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }
}
