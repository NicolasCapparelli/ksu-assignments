package A7;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 */
public class Occurrences {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] entered = new int[10];

        for (int i = 0; i < entered.length; i++){
            entered[i] = scanner.nextInt();
        }

        System.out.print("Entered integers");
        for (int i : entered){
            System.out.print(i + " ");
        }

        count(entered);
    }

    private static void count(int[] arr){

        int[] done = new int[arr.length-1];
        for(int i = 0; i < arr.length; i++){

            int counter = 0;


            for(int j = 0; j < arr.length; j++){
                if (arr[i] == arr[j]){
                    counter++;
                }
            }

            //if (Arrays.asList(done).contains(Integer.arr[i]))
            System.out.print("\n" + arr[i] + " occurred " + counter + " times");

            done[i] = arr[i];
        }

    }
}
