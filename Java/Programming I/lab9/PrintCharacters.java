package lab9;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 9
 */

public class PrintCharacters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char inputtedValue = scanner.next().charAt(0);
        char inputtedValueTwo = scanner.next().charAt(0);

        printChars(inputtedValue, inputtedValueTwo);

    }

    private static void printChars(char ch1, char ch2){

        int count = 1;

        for (int x = (int)ch1; x <= (int)ch2; x++){
            System.out.print((char)x);

            if (count % 5 == 0){
                System.out.print("\n");
            }
            count++;
        }

    }
}
