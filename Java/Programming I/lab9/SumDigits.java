package lab9;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 9
 */

public class SumDigits {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int inputtedValue = scanner.nextInt();

        System.out.println(sumDigits(inputtedValue));

    }

    private static int sumDigits(int number){

        int total = 0;

        while (number > 0) {
            total += number % 10;
            number = number / 10;
        }
        return total;
    }
}
