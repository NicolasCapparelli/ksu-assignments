package lab9;

import java.util.Random;
import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 9
 */

public class GuessNumber {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int secret = randomValue();
        int guess = -1;
        char keepGoing = 'y';

        while (keepGoing != 'n'){
            while(guess != secret){
                guess = askUser();
                System.out.println(checkGuess(secret, guess));
            }

            System.out.println("Keep playing? y/n?");
            keepGoing = scanner.next().charAt(0);
            secret = randomValue();
        }

    }

    private static int randomValue(){
        return new Random().nextInt(20) + 1;
    }

    private static int askUser(){
        System.out.println("What is your guess?");
        return scanner.nextInt();
    }

    private static String checkGuess(int value, int guess){
        if (value > guess){
            return "Too low!";
        } else if (value < guess) {
            return "Too high!";
        }
        else {
            return "Correct!";
        }
    }
}
