package pastlabs;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 5
 */

public class ProcessGrades {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        int[] grades = new int[4];

        for (int i = 0; i < grades.length; i++){

            grades[i] = scanner.nextInt();

            // Check that the inputted value is in the correct range
            if (grades[i] < 0 || grades[i] > 100){
                System.out.println("Please ensure that values only range from 0 to 100");
                System.exit(1);
            }
                    }

        // Print grades
        System.out.println("You entered: " + grades[0] + ", " + grades[1] + ", " + grades[2] + ", " + grades[3]);

        // Sorts the array from low to high
        Arrays.sort(grades);

        System.out.println("Highest grade: " + grades[3]);
        System.out.println("Lowest grade: " + grades[0]);
        System.out.println("Average grade: " + String.format("%.2f", ((double)Arrays.stream(grades).sum() / (double)grades.length)));



    }
}
