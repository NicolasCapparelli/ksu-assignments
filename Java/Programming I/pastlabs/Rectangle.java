package pastlabs;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 7
 */
public class Rectangle {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char symb = scanner.next().charAt(0);

        int numb = scanner.nextInt();

        for (int i = 0; i < numb; i++){

            for (int j = 0; j < numb; j++){
                System.out.print(symb);
            }
            System.out.println();

        }

    }

}
