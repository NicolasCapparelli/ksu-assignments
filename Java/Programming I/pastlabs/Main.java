package pastlabs;

import okhttp3.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Created by Nico on 6/15/2017.
 */
public class Main {

    // pastlabs.Main
    public static void main(String[] args) throws IOException, JSONException {

        String email = "capparellinicog@hotmail.com";

        String pwd = "AndrewIsGhey1";

        String token = getAuth(email, pwd);

        System.out.println(token);

        int respCode = changePwd(email, token, pwd, "AndrewIsGhey2");

        System.out.println(respCode);

    }

//// Constants ////

    // OkHttpClient needed to make requests
    private static final OkHttpClient client = new OkHttpClient();

    // Media Type for post requests
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");


///// Methods to test /////

    // Registration
    private static void registerUser(String name, String email, String pwd) throws IOException {

        // URI of the API
        String URL = "http://138.197.81.220:8080/api/v1/users";

        // Creates JSON payload, stills needs to be built with values
        JSONObject payload = new JSONObject();

        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        payload.put("name", name);
        payload.put("email", email);
        payload.put("password", pwd);

        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(payload));

        // Building the request itself with the given attributes
        Request request = new Request.Builder()
                .url(URL)
                .post(params)
                .build();

        // Firing the request and receiving the response as a variable
        Response response = client.newCall(request).execute();

        // Response code
        int respCode = response.code();

        System.out.println(String.valueOf(respCode));
    }

    // Get token
    private static String getAuth(String email, String pwd) throws IOException {
        String token = "";

        // URI of the API
        String URL = "http://138.197.81.220:8080/api/v1/authenticate";

        // Putting the credentials into one string
        String credentials = email + ":" + pwd;

        // Encoding/Encrypting that string with Base64
        String basic = "Basic " + java.util.Base64.getEncoder().encodeToString(credentials.getBytes());

        // Empty body
        RequestBody params = RequestBody.create(JSON, "");

        // Building the request itself with the given attributes
        Request request = new Request.Builder()
                .url(URL)
                .header("Authorization", basic)
                .post(params)
                .build();

        // Firing the request and receiving the response as a variable
        Response response = client.newCall(request).execute();

        // Response code
        int respCode = response.code();

        // Turning response to a string then creating a JSON object with the response string
        String resp = response.body().string();
        JSONObject respJson = new JSONObject(resp);

        // If request is successful
        if (respCode == 200){
            token = respJson.getString("token");
        }
        else {
            token = "Error";
        }

        // Returns token
        return token;
    }

    // Changes the user's password
    private static int changePwd(String email, String authToken, String oldPwd, String newPwd) throws IOException {

        // URI of the API
        String URL = "http://138.197.81.220:8080/api/v1/users/" + email;

        // Creates JSON payload, stills needs to be built with values
        JSONObject payload = new JSONObject();

        // Building the payload JSON object
        payload.put("password", oldPwd);
        payload.put("newPassword", newPwd);

        // Creates the request body
        RequestBody params = RequestBody.create(JSON, String.valueOf(payload));

        // Building the request itself with the given attributes
        Request request = new Request.Builder()
                .url(URL)
                .header("x-access-token", authToken)
                .put(params)
                .build();

        // Firing the request and receiving the response as a variable
        Response response = client.newCall(request).execute();

        // Returns the response code
        return response.code();
    }

    // Reset pwd step one (Generates token)
    private static int resetPwdOne(String email) throws IOException {

        // URL
        String URL = "http://138.197.81.220:8080/api/v1/users/" + email + "/password";

        // Creates JSON payload, stills needs to be built with values
        JSONObject payload = new JSONObject();

        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        payload.put("", "");

        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(payload));

        // Building the request itself with the given attributes
        Request request = new Request.Builder()
                .url(URL)
                .post(params)
                .build();

        // Firing the request and receiving the response as a variable
        Response response = client.newCall(request).execute();

        return response.code();
    }


    // Step two of password change, after the user has received their pwd change token
    private static int resetPwdTwo(String changeToken, String email, String newPwd) throws IOException {

        // URL
        String URL = "http://138.197.81.220:8080/api/v1/users/" + email + "/password";

        // Creates JSON payload, stills needs to be built with values
        JSONObject payload = new JSONObject();

        // Building the payload JSON object (must be surrounded in try/catch for some reason)
        payload.put("token", changeToken);
        payload.put("password", newPwd);


        // Creates the request body, which I guess is just the params because Requests don't typically have a body
        RequestBody params = RequestBody.create(JSON, String.valueOf(payload));

        // Building the request itself with the given attributes
        Request request = new Request.Builder()
                .url(URL)
                .post(params)
                .build();

        // Firing the request and receiving the response as a variable
        Response response = client.newCall(request).execute();

        // Return response code
        return response.code();
    }


    // Get watchlist
    private static ArrayList<String> getWatchlist(String email, String token) throws JSONException, IOException {


        ArrayList<String> symbols = new ArrayList<>();

        // URI
        String URI = "http://138.197.81.220:8080/api/v1/users/" + email + "/watchlist";

        // Creating the GET request
        Request request = new Request.Builder()
                .url(URI)
                .header("x-access-token", token)
                .build();

        // Telling the client to execute the request and put the response in the response variable
        Response response = client.newCall(request).execute();

        // Response code
        int respCode = response.code();

        // Turning response into a string and then building a JSON object with it
        String resp = response.body().string();
        JSONObject respJson = new JSONObject(resp);

        // If the request succeeds
        if (respCode == 200){
            // Putting the JSON Array into a variable
            JSONArray watchArray = respJson.getJSONArray("watchlist");

            // Iterating through the JSON array
            for (int i=0; i<watchArray.length(); i++) {

                // Putting the values into the symbols arrayList
                symbols.add( watchArray.getString(i) );
            }

        }

        // If the request fails
        else {
            // Add String ERROR to the array
            symbols.add("ERROR");
        }

        // Return symbols array
        return symbols;
    }

}
