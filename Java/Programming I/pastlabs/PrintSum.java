package pastlabs;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 6
 */
public class PrintSum {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        int inputtedVal = scanner.nextInt();

        if (inputtedVal < 1 || inputtedVal > 100) {
            System.out.println("Invalid input. Try again.");
            System.exit(1);
        }

        else {
            System.out.println("You entered: " + inputtedVal);

            int firstNumber = 0, secondNumber = 1, i = 0;

            while (i < inputtedVal){
                int sumOfNumbers = firstNumber + secondNumber;
                firstNumber = secondNumber;
                secondNumber = sumOfNumbers;
                i++;
            }
            System.out.println("Sum of values: " + firstNumber);
        }
    }
}
