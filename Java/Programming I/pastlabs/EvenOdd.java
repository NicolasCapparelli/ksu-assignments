package pastlabs;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 6
 */
public class EvenOdd {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int start = 50;

        System.out.print("Even numbers between 50 and 100: ");

        while(start < 101){

            if (start != 100) {
                System.out.print(start + ",");
            } else {
                System.out.print(start);
            }
            start += 2;
        }

        start = 51;
        System.out.print("\nOdd numbers between 50 and 100: ");

        while (start < 100) {
            if (start != 99) {
                System.out.print(start + ",");
            } else {
                System.out.print(start);
            }
            start += 2;
        }

    }

}
