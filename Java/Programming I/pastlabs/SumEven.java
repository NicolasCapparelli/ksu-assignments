package pastlabs;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 7
 */
public class SumEven {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        int inputtedValue;

        do {
            inputtedValue = scanner.nextInt();
        } while (inputtedValue < 20 || inputtedValue > 60);

        System.out.println("Entered value: " + inputtedValue);

        int sum = 0;

        for (int i = 2; i <= inputtedValue; i+=2){
            sum+=i;
        }

        System.out.println("Sum of even numbers between 2 and " + inputtedValue + " is " + sum);
    }
}
