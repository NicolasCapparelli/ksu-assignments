package pastlabs;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 4
 */

public class NextMeeting {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        Map<Integer,String> hm = new HashMap<>();
        hm.put(0, "Sunday");
        hm.put(1, "Monday");
        hm.put(2, "Tuesday");
        hm.put(3, "Wednesday");
        hm.put(4, "Thursday");
        hm.put(5, "Friday");
        hm.put(6, "Saturday");

        int today = scanner.nextInt();
        int deltaDay = scanner.nextInt();

        System.out.println("Today is: " + hm.get(today));
        System.out.println("Days to the meeting is " + deltaDay + " days");

        int answer = (today + deltaDay) % 7;

        System.out.println("Meeting day is: " + hm.get(answer));
    }
}
