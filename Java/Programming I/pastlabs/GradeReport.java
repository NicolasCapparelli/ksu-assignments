package pastlabs;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 4
 */

public class GradeReport{

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int inputtedGrade = input.nextInt();

        if (inputtedGrade == 100) {
            System.out.println("That grade is a perfect score. Well done.");
        }

        else if (inputtedGrade >= 90 && inputtedGrade <= 99) {
            System.out.println("That grade is well above average. Excellent work.");
        }

        else if (inputtedGrade >= 80 && inputtedGrade <= 89) {
            System.out.println("That grade is well above average. Nice Job.");
        }

        else if (inputtedGrade >= 70 && inputtedGrade <= 79) {
            System.out.println("That grade is average work");
        }

        else if (inputtedGrade >= 60 && inputtedGrade <= 69) {
            System.out.println("That grade is not good, you should seek help!");
        }

        else {
            System.out.println("This grade is not passing");
        }
    }
}
