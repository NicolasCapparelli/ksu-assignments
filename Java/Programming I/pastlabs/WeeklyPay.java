package pastlabs;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 4
 */

public class WeeklyPay {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int hours = scanner.nextInt();
        int extraHours;
        double gross;

        if (hours > 40){
            extraHours = hours - 40;
            gross = (extraHours * 15) + (40*10);
        } else {
            gross = hours*10;
        }

        System.out.println("You entered " + hours + " hours");
        System.out.println("Gross earnings is $" + gross);

    }
}
