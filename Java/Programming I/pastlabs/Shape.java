package pastlabs;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 7
 */
public class Shape {
    public static void main(String[] args) {

        int lines = 8;
        int star = 1;
        int spaces = 7;

        for (int rows = 0; rows < lines; rows++){
            for (int i = 0; i < spaces; i++){
                System.out.print(" ");
            }

            for (int j = 0; j < star; j++){
                System.out.print("*");
            }

            spaces--;
            star += 2;
            System.out.println();
        }

    }
}
