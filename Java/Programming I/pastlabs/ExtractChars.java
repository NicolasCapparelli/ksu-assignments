package pastlabs;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 6
 */
public class ExtractChars {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String inputtedVal = scanner.next();

        int i = 0;

        System.out.println("Entered string: " + inputtedVal);
        while(i < inputtedVal.length() ){
            System.out.println("Character #" + (i+1) + ": " + inputtedVal.charAt(i));
            i++;
        }
    }
}
