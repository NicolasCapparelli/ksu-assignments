package pastlabs;

import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 5
 */
public class CheckPoint {

    public static void main(String[] main) {

        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();
        int y = scanner.nextInt();

        if (x > 0 && y > 0) {
            System.out.println("(" + x + "," + y + ") is in the first quadrant");
        }
        else if (x < 0 && y > 0){
            System.out.println("(" + x + "," + y + ") is in the second quadrant");
        }

        else if (x < 0 && y < 0) {
            System.out.println("(" + x + "," + y + ") is in the third quadrant");
        }

        else if (x > 0 && y < 0) {
            System.out.println("(" + x + "," + y + ") is in the fourth quadrant");
        }

        else if (x == 0 && y != 0) {
            System.out.println("(" + x + "," + y + ") is on the y axis");
        }

        else if (x != 0 && y == 0){
            System.out.println("(" + x + "," + y + ") is on the x axis");
        }

        else {
            System.out.println("(" + x + "," + y + ") is the origin point");
        }
    }
}
