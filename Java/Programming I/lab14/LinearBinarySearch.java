package lab14;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 14
 */

public class LinearBinarySearch {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int[] inputArr = new int[8];

        for (int i = 0; i < inputArr.length; i++) {
            inputArr[i] = scanner.nextInt();
        }

        int target = new Random().nextInt(inputArr.length);

        System.out.println("Array Values:\t" + Arrays.toString(inputArr));
        System.out.println("Target Value:\t" + inputArr[target]);
        System.out.println();

        int lTimes = linearSearch(inputArr, inputArr[target]);
        int bTimes = binarySearch(inputArr, inputArr[target]);

        System.out.println("Linear Search Comparisons:\t" + lTimes);
        System.out.println("Binary Search Comparisons:\t" + bTimes);
    }

    private static int linearSearch(int[] searchMe, int target) {
        for (int i = 0; i < searchMe.length; i++)
            if (searchMe[i] == target)
                return i + 1;
        return -1;
    }

    private static int binarySearch(int[] searchMe, int target) {
        Arrays.sort(searchMe);

        int start = 0;
        int end = searchMe.length - 1;
        int steps = 1;

        while (start <= end) {
            int mid = (start + end) / 2;
            if (target == searchMe[mid]) {
                return steps;
            }
            if (target < searchMe[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
            steps++;
        }
        return -1;
    }

}
