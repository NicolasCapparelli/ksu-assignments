package lab14;

import java.util.Arrays;
import java.util.Random;


/**
 * Class: CSE 1321L
 * Section: TN
 * Term: Fall
 * Instructor: Kristin Hegna
 * Name: Nicolas Capparelli
 * Lab#: 14
 */

public class SimpleSort {

    public static void main(String[] args) {
        int[] bubbleArr = new int[50];
        int[] insertionArr = new int[50];
        int[] selectionArr = new int[50];

        Random rand = new Random();

        for (int i = 0; i < bubbleArr.length; i++) {
            bubbleArr[i] = rand.nextInt(100);
        }

        for (int i = 0; i < insertionArr.length; i++) {
            insertionArr[i] = rand.nextInt(100);
        }

        for (int i = 0; i < selectionArr.length; i++) {
            selectionArr[i] = rand.nextInt(100);
        }

        System.out.println();
        System.out.println("Bubble Array Values\t" + Arrays.toString(bubbleArr));
        System.out.println("Insertion Array Values\t" + Arrays.toString(insertionArr));
        System.out.println("Bubble Array Values\t" + Arrays.toString(selectionArr));
        System.out.println();

        int bubbleSwaps = bubbleSort(bubbleArr);
        int insertionSwaps = insertionSort(insertionArr);
        int selectionSwaps = selectionSort(selectionArr);

        System.out.println("Bubble Sorted Values:\t" + Arrays.toString(bubbleArr));
        System.out.println("Bubble Sort Swaps:\t" + bubbleSwaps);
        System.out.println();

        System.out.println("Insertion Sorted Values:\t" + Arrays.toString(insertionArr));
        System.out.println("Insertion Sort Swaps:\t" + insertionSwaps);
        System.out.println();

        System.out.println("Selection Sorted Values:\t" + Arrays.toString(selectionArr));
        System.out.println("Selection Sort Swaps:\t" + selectionSwaps);

    }

    private static int bubbleSort(int[] sortMe){

        int steps = 0;

        int n = sortMe.length;
        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (sortMe[j] > sortMe[j + 1]) {
                    int temp = sortMe[j];
                    sortMe[j] = sortMe[j + 1];
                    sortMe[j + 1] = temp;
                    steps++;
                }
            }
        }

        return steps;
    }

    private static int insertionSort(int[] sortMe){

        int steps = 0;

        int n = sortMe.length;
        for (int i=1; i<n; ++i) {
            int key = sortMe[i];
            int j = i-1;
            while (j>=0 && sortMe[j] > key) {
                sortMe[j+1] = sortMe[j];
                j = j-1;
                steps++;
            }
            sortMe[j+1] = key;
        }

        return steps;
    }

    private static int selectionSort(int[] sortMe){

        int steps = 0;
        int n = sortMe.length;

        for (int i = 0; i < n-1; i++) {
            int min_idx = i;
            for (int j = i+1; j < n; j++) {
                if (sortMe[j] < sortMe[min_idx])
                    min_idx = j;
            }

            int temp = sortMe[min_idx];
            sortMe[min_idx] = sortMe[i];
            sortMe[i] = temp;
            steps++;
        }

        return steps;
    }

}


